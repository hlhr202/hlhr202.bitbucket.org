module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		importlib: {
			main: {
				files: [
					{
						cwd: './bower_components/bootstrap/dist/',
						src: '**/*',
						dest: './public/bootstrap/',
						expand: true,
					},
					{
						src: ['./bower_components/jquery/dist/*'],
						dest: './public/jquery/',
						flatten: true,
						expand: true,
					}
				]
			}
		},
		imagemin: {
			dist: {
				options: {
					optimizationLevel: 3
				},
				files: [{
					expand: true,
					cwd: './images/unprocessed',
					src: ['**/*.{png,jpg,jpeg}'],
					dest: './public/images/'
				}]
			}
		},
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'public/',
					src: ['*.css', '!*.min.css'],
					dest: 'public/',
					ext: '.min.css'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['importlib']);
	grunt.registerTask('img', ['imagemin']);
	grunt.registerTask('css', ['cssmin']);
}